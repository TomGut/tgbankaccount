package pl.codementors;

public class BankAccount {

    //dla utrudnienia możemy przelewać między kontami, tworzyć child konta na np. studenckie, dziecięce itd.

    private int balance;
    private int debt;

    //debet na minusie i nie wolno go przekroczyć
    private static final int DEFAULT_MAX_DEBT = 200;

    public BankAccount() {
        this.balance = 0;
        this.debt = 0;
    }

    public BankAccount(int initialBalance) {
        this.balance = initialBalance;
    }

    public BankAccount(int initialBalance, int debt) {
        if(initialBalance - debt > DEFAULT_MAX_DEBT * -1){
            this.debt = debt;
            this.balance = initialBalance - debt;
        }else{
            System.out.println("Max debt on account is -" + DEFAULT_MAX_DEBT);
        }
    }

    public int getBalance() {
        return this.balance;
    }

    public void setBalance(int balance) {
        if(balance < balance - debt){
            System.out.println("Need more to pay your debts");
        }else{
            this.balance = balance - debt;
            this.debt = 0;
        }
    }

    public int getDebt() {
        return this.debt;
    }

    public void setDebt(int debt) {
        if(balance - debt < DEFAULT_MAX_DEBT * -1){
            System.out.println("Too big debt to be set");
        }else {
            this.debt = debt;
            this.balance = balance - debt;
        }
    }

    public static int getDefaultMaxDebt() {
        return DEFAULT_MAX_DEBT;
    }

    /**
     * Wplaca pieniadze i zwraca aktualny stan konta.
     */
    public int deposit(int amount) {
        balance = balance + amount;
        return balance;
    }

    /**
     * Wyplaca pieniadze i zwraca aktualny stan konta.
     */
    public int withdraw(int amount) {

        //TODO wrzucić ify
            balance = balance - amount;

        return balance;
    }

    @Override
    public String toString() {
        // TODO
        return "0";
    }
}
