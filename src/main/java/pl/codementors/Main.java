package pl.codementors;

public class Main {

    public static void main(String[] args) {

        BankAccount mainAccount = new BankAccount(1400, 100);
        System.out.println("Initianl balance " + (mainAccount.getBalance() + mainAccount.getDebt()));
        System.out.println("Initial debt");
        System.out.println(mainAccount.getDebt());
        System.out.println("Balance after initial debt");
        System.out.println(mainAccount.getBalance());
        mainAccount.setDebt(50);
        System.out.println("Balance after setDebt " + mainAccount.getDebt());
        System.out.println(mainAccount.getBalance());
        mainAccount.deposit(300);
        System.out.println("Balance after deposit");
        System.out.println(mainAccount.getBalance());
        mainAccount.setBalance(2000);
        System.out.println("Account after setBalance - debt is taken");
        System.out.println(mainAccount.getBalance());
        System.out.println(mainAccount.getDebt());
    }
}
